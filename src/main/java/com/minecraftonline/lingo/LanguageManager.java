package com.minecraftonline.lingo;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * The language manager
 *
 * <p>Allows commands to dynamically receive translated messages
 * from the server. Rather than using language files with string-
 * based templates, we use an {@code interface} with methods which can
 * take in arbitrary parameters and return rich objects.</p>
 */
public abstract class LanguageManager<T extends Language> {
    private final Map<Locale, T> languages = new HashMap<>();
    private final Map<Locale, Locale> cache = new HashMap<>();

    /**
     * Get the default {@link Locale} for this {@link LanguageManager}.
     *
     * @return the locale
     */
    public abstract Locale defaultLocale();

    /**
     * Add a polymorphic {@link Language} class to the map of supported languages.
     *
     * @param locale The {@link Locale} the language is designed for, e.g. {@code en_GB}
     * @param language The language class
     */
    public void add(Locale locale, T language) {
        languages.put(locale, language);
        // It's cheaper to just clear the cache than to try to selectively remove now-invalid entries
        // We do this because if we previously cached "fr_FR" and then later add an entry for just "fr,"
        // "fr_FR" would still lead to whatever the default was, which is undesirable.
        cache.clear();
    }

    /**
     * Attempt to resolve a {@link Locale} to the most specific subset that is in the map of supported
     * languages. For example, if {@code en} is in the map and you pass {@code en_US} to this method,
     * you'd expect for it to return {@code en}.
     *
     * @param locale The locale to resolve
     * @return The resolved locale
     */
    private Locale resolveLocale(Locale locale) {
        // Step 1: Full country (en_GB) -- Minecraft doesn't use any of the variants
        Locale fullLocale = new Locale(locale.getLanguage(), locale.getCountry());
        if (languages.containsKey(fullLocale)) {
            return fullLocale;
        }
        if (cache.containsKey(fullLocale)) {
            return cache.get(fullLocale);
        }
        // Step 2: Just language (en)
        Locale justLanguage = new Locale(locale.getLanguage());
        if (languages.containsKey(justLanguage)) {
            if (!cache.containsKey(fullLocale)) {
                cache.put(fullLocale, justLanguage);
            }
            return justLanguage;
        }
        // Step 3: Fallback to default
        if (!cache.containsKey(fullLocale)) {
            cache.put(fullLocale, defaultLocale());
        }
        return defaultLocale();
    }

    /**
     * Get the appropriate {@link Language} class for a {@link Locale}.
     *
     * @param locale The locale to resolve and look up
     * @return the appropriate language class
     */
    public T translations(Locale locale) {
        return languages.get(resolveLocale(locale));
    }

}
